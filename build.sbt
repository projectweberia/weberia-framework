//enablePlugins(ScalaJSPlugin)

organization := "Project Weberia"

name := "weberia-framework"

version := "1.0.0"

scalaVersion := "2.11.7"

resolvers ++= Seq(
  "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/",
  "nxparser-repo" at "http://nxparser.googlecode.com/svn/repository/",
  "Bigdata releases" at "http://systap.com/maven/releases/",
  "Sonatype OSS Releases" at "https://oss.sonatype.org/content/repositories/releases",
  "apache-repo-releases" at "http://repository.apache.org/content/repositories/releases/",
  "Scalaz Bintray Repo" at "http://dl.bintray.com/scalaz/releases"
)

libraryDependencies ++= {

  val (akkaV, streamHttp, blazeG) = ("2.3.11", "1.0-RC4", "1.5.1")

  // remember, %% means scala-major-version, so %% "bigdata" below will 
  // result error since there is no bigdata_2.11 version. use % instead
  // see: http://www.scala-sbt.org/0.13/tutorial/Library-Dependencies.html

  Seq(
    //"org.parboiled"           %% "parboiled"                    % "2.1.0",
    "org.scala-lang.modules"  %% "scala-parser-combinators"     % "1.0.4",
    "com.typesafe.akka"       %% "akka-stream-experimental"     % streamHttp,
    "com.typesafe.akka"       %% "akka-http-experimental"       % streamHttp,
    "com.typesafe.akka"       %% "akka-http-core-experimental"  % streamHttp,
    "com.typesafe.akka"       %% "akka-actor"                   % akkaV,
    "com.bigdata"             % "bigdata"                      % blazeG,
    "com.lihaoyi"             %% "scalatags"                    % "0.5.2",
    "com.typesafe.akka"       %% "akka-testkit"                 % akkaV     % "test",  
    "org.specs2"              %% "specs2-core"                  % "3.6.1"  % "test"
  )

}

scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature")

lazy val root = (project in file(".")).enablePlugins(PlayScala)
